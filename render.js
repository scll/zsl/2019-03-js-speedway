class Render {
    constructor(ctx, angle1, angle2, angle3, angle4, tab1, tab2, tab3, tab4, defaultPos1, defaultPos2, defaultPos3, defaultPos4) {
        const pi = Math.PI
        ctx.strokeStyle = "rgba(0,0,0,1)";
        ctx.lineWidth = 5;
        ctx.beginPath();
        ctx.rect(0, 0, 600, 400);
        ctx.fillStyle = ctx.createPattern(document.getElementById("chairs"), "repeat");
        ctx.stroke();
        ctx.fill();

        ctx.beginPath();
        ctx.arc(400, 200, 180, 1.5 * pi, 0.5 * pi);
        ctx.arc(200, 200, 180, 0.5 * pi, 1.5 * pi);
        ctx.fillStyle = ctx.createPattern(document.getElementById("road"), "repeat");
        ctx.closePath();
        ctx.stroke();
        ctx.fill();

        ctx.beginPath();
        ctx.arc(400, 200, 70, 1.5 * pi, 0.5 * pi);
        ctx.arc(200, 200, 70, 0.5 * pi, 1.5 * pi);
        ctx.fillStyle = ctx.createPattern(document.getElementById("grass"), "repeat");
        ctx.closePath();
        ctx.stroke();
        ctx.fill();

        ctx.lineWidth = 4
        var movement = function (tabGracza, defaultPos, angle, idGracza) {
            const promien = 5
            const wektor_y = Math.sin(angle) * promien
            const wektor_x = Math.cos(angle) * promien
            const posx = tabGracza[tabGracza.length - 1][0] + wektor_x
            const posy = tabGracza[tabGracza.length - 1][1] + wektor_y
            var joh = [posx, posy]
            tabGracza.push(joh)
            for (let i = 0; i < tabGracza.length; i++) {
                if (tabGracza.length == 30) { tabGracza.shift() }
                ctx.beginPath();
                if (tabGracza[i - 1]) {
                    ctx.moveTo(tabGracza[i - 1][0], tabGracza[i - 1][1]);
                }
                else {
                    if (defaultPos) {
                        ctx.moveTo(300, 300)
                    }
                }
                if (i >= 10) { var opacity = "5" + i } //dodatkowe 0.5pkt
                else if (i >= 15) { var opacity = "8" + i }
                else if (i >= 18) { var opacity = "99" }
                else { var opacity = i / 2 }
                if (idGracza == 0) { var kolor = "255,0,0" }
                if (idGracza == 1) { var kolor = "0,255,0" }
                if (idGracza == 2) { var kolor = "0,0,255" }
                if (idGracza == 3) { var kolor = "255,255,0" }
                ctx.strokeStyle = "rgba(" + kolor + ",0." + opacity + ")";//dodatkowe 0.5pkt

                if (i == tabGracza.length - 1) {
                    ctx.save();
                    var img = document.getElementById("motorbike" + idGracza);
                    ctx.translate(posx, posy)
                    ctx.rotate(angle)
                    ctx.drawImage(img, -10, -5, 30, 10);
                    ctx.restore();
                }
                defaultPos = true
                ctx.lineTo(tabGracza[i][0], tabGracza[i][1])
                ctx.stroke();
                ctx.closePath();
            }
            ctx.beginPath();
            ctx.arc(400, 200, 180, 1.5 * pi, 0.5 * pi);
            ctx.arc(200, 200, 180, 0.5 * pi, 1.5 * pi);
            if (!ctx.isPointInPath(posx, posy)) {
                moving.going[idGracza] = false
                moving.deaths.push(idGracza)
            }
            ctx.beginPath();
            ctx.arc(400, 200, 70, 1.5 * pi, 0.5 * pi);
            ctx.arc(200, 200, 70, 0.5 * pi, 1.5 * pi);
            if (ctx.isPointInPath(posx, posy)) {
                moving.going[idGracza] = false
                moving.deaths.push(idGracza)
            }
            ctx.beginPath();
            ctx.rect(300, 20, 10, 150)
            if (ctx.isPointInPath(posx, posy)) { //checkpoint
                moving.correctLaps[idGracza] = true
            }
            ctx.beginPath();
            ctx.rect(299, 250, 10, 150)
            if (ctx.isPointInPath(posx, posy)) { //przejazd przez start
                if (moving.correctLaps[idGracza] == true) {
                    moving.laps[idGracza] += 1
                    moving.correctLaps[idGracza] = false
                    var uf = true
                    for (let i = 0; i < 4; i++) {
                        if (moving.laps[idGracza] < moving.laps[i]) {
                            uf = false
                        }
                    }
                    if (uf) document.getElementById("lapsRemaining").innerHTML = "Okrążenia do końca: " + (5 - moving.laps[idGracza])
                    if (moving.laps[idGracza] == 5) { //ilosc okrazen
                        moving.win = idGracza
                        console.log(moving.win)
                    }
                }
            }

            if (moving.playersCount - 1 == moving.deaths.length && moving.playersCount != 1) { //sprawdzanie czy reszta zyje
                var johh = true
                for (let i = 0; i < moving.deaths.length; i++) {
                    if (idGracza == moving.deaths[i]) {
                        johh = false
                    }
                }
                if (johh) moving.win = idGracza
            }

        }
        if (moving.going[0] == true) { movement(tab1, defaultPos1, angle1, 0) }
        if (moving.going[1] == true) { movement(tab2, defaultPos2, angle2, 1) }
        if (moving.going[2] == true) { movement(tab3, defaultPos3, angle3, 2) }
        if (moving.going[3] == true) { movement(tab4, defaultPos4, angle4, 3) }
    }
};
