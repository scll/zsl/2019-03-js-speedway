document.addEventListener("DOMContentLoaded", function () {
    var beforeClick = function (that, i) {
        afterClick(that, i);
    }
    var afterClick = function (that, i) {
        that.addEventListener("keypress", function (e) {
            console.log(that)
            if (e.keyCode) {
                var correctKeyCode = true
                for (let j = 0; j < 4; j++) {
                    if (moving.keyCodes[j] == e.keyCode && i != j)
                        correctKeyCode = false
                }
                if (correctKeyCode == true) {
                    that.innerHTML = String.fromCharCode(e.keyCode)
                    moving.keyCodes[i] = e.keyCode
                }
                var old_element = that;
                var new_element = old_element.cloneNode(true);
                old_element.parentNode.replaceChild(new_element, old_element);
                beforeClick(new_element, i)
            }
        })
    }
    for (let i = 0; i < 4; i++) {
        let that = document.getElementsByClassName("buttonKeyCode")[i]
        document.getElementsByClassName("buttonKeyCode")[i].addEventListener("click", beforeClick(that, i))
        document.getElementsByTagName("input")[i].addEventListener("click", function () {
            if (this.checked == true) {
                document.getElementsByClassName("buttonKeyCode")[i].disabled = false
                moving.going[i] = true
            }
            if (this.checked == false) {
                document.getElementsByClassName("buttonKeyCode")[i].disabled = true
                moving.going[i] = false
                moving.keyCodes[i] = null
                document.getElementsByClassName("buttonKeyCode")[i].innerHTML = "wybierz klawisz"
            }
        })
    }
    document.getElementById("startGame").addEventListener("click", function () { //start gry
        var correctStart = 0
        var playersCount1 = 0
        for (let i = 0; i < 4; i++) { //czy wszyscy gracze mają przypisane przyciski
            if (moving.going[i] == true && moving.keyCodes[i] != null) { correctStart++; playersCount1++ }
            if (moving.going[i] == false && moving.keyCodes[i] == null) { correctStart++ }
        }
        if (correctStart == 4) {
            moving.playersCount = playersCount1
            init()
            document.getElementById("overlay").style.visibility = "hidden"
        }
        else {
            alert("jak to zrobić poprawnie i jak to potem odzyskać???")
        }
    })
})
var init = function () {
    const cnv = document.getElementById("cnv")
    const ctx = cnv.getContext("2d");
    const defaultPos1 = [300, 290]
    const defaultPos2 = [300, 310]
    const defaultPos3 = [300, 330]
    const defaultPos4 = [300, 350]
    const tab1 = [defaultPos1]
    const tab2 = [defaultPos2]
    const tab3 = [defaultPos3]
    const tab4 = [defaultPos4]
    var uf1 = 0
    var uf2 = 0
    var uf3 = 0
    var uf4 = 0
    var angle1 = 0;
    var angle2 = 0;
    var angle3 = 0;
    var angle4 = 0;

    var canv = function () {
        if (moving.win == undefined) {
            var render1 = new Render(ctx, angle1, angle2, angle3, angle4, tab1, tab2, tab3, tab4, defaultPos1, defaultPos2, defaultPos3, defaultPos4)
        }
        else {
            alert("Wygrywa gracz " + (moving.win + 1))
            clearInterval(inetrval1)
        }
    }
    var inetrval1 = setInterval(canv, 50)
    document.body.addEventListener("keypress", function (e) {
        if (e.keyCode == moving.keyCodes[0]) {
            uf1 -= 0.1
            if (uf1 < 0) { uf1 = 2 }
            angle1 = uf1 * Math.PI
        }
        else if (e.keyCode == moving.keyCodes[1]) {
            uf2 -= 0.1
            if (uf2 < 0) { uf2 = 2 }
            angle2 = uf2 * Math.PI
        }
        else if (e.keyCode == moving.keyCodes[2]) {
            uf3 -= 0.1
            if (uf3 < 0) { uf3 = 2 }
            angle3 = uf3 * Math.PI
        }
        else if (e.keyCode == moving.keyCodes[3]) {
            uf4 -= 0.1
            if (uf4 < 0) { uf4 = 2 }
            angle4 = uf4 * Math.PI
        }
    })
}
var moving = {
    going: [false, false, false, false],
    keyCodes: [null, null, null, null],
    deaths: [],
    playersCount: 0,
    laps: [0, 0, 0, 0],
    correctLaps: [false, false, false, false],
    win: undefined
}